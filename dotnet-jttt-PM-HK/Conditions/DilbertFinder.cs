﻿using System;
using System.Linq;
using System.Text;
using System.Net;
using System.Runtime.Serialization;
using HtmlAgilityPack;
using System.Xml;

namespace dotnet_jttt_PM_HK.Conditions
{
    using Interfaces;
    using Exceptions;
    using Models;

    [Serializable]
    public class DilbertFinder : ICondition
    {
        private string _url;

        public string Url { get => _url; set => _url = value; }

        public DilbertFinder()
        {
            _url = "http://dilbert.com/strip/";
        }

        public DilbertFinder(SerializationInfo info, StreamingContext context)
        {
            _url = "http://dilbert.com/strip/";
        }

        private string GetHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(_url+ DateTime.Now.ToString("yyyy-MM-dd")));

                return html;
            }
        }

        private string GetImageUri()
        {
            var doc = new HtmlDocument();
            var pageHtml = GetHtml();
            doc.LoadHtml(pageHtml);
            var nodes = doc.DocumentNode.Descendants("img");
            var findNode = nodes.FirstOrDefault(
                node =>
                {
                    string alt = node.GetAttributeValue("alt", "");
                    string src = node.GetAttributeValue("src", "");
                    return alt.Contains("Dilbert by Scott Adams") && src.Contains("http");
                }
            );
            if (findNode is null)
                return "";
            return findNode.GetAttributeValue("src", "");
        }

        public override bool Check()
        {
            try
            {
                result = GetImageUri();
                if ((string)result == "")
                {
                    CheckInformation = "Nie znaleziono dzisiejszego obrazka Dilberta";
                    return false;
                }
                var info = new ImageInfo
                {
                    ImgUrl = (string)result,
                    Description = ""
                };
                result = info;
                return true;
            }
            catch(Exception ex)
            {
                throw new JtttException("Nie udało się połączyć ze stroną http://dilbert.com/", "Exception : DilbertFinder::Check()", ex);
            }
        }

        public override string ToString()
        {
            return $"[Dilbert {DateTime.Now.ToString("yyyy-MM-dd")}]";
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("DilbertFinder : SerializationInfo");
        }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (!isEmptyElement)
            {
                reader.ReadEndElement();
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
        }
    }
}
