﻿using dotnet_jttt_PM_HK.Exceptions;
using dotnet_jttt_PM_HK.Interfaces;
using dotnet_jttt_PM_HK.Models;
using dotnet_jttt_PM_HK.Static;
using System;
using System.Runtime.Serialization;
using System.Windows;
using System.Xml;

namespace dotnet_jttt_PM_HK.Conditions
{


    [Serializable]
    public class WeatherConditions : ICondition
    {
        private string currentLocation;
        private int tempThreshold;
        
        public int TempThreshold { get => tempThreshold; set => tempThreshold = value; }
        public string CurrentLocation { get => currentLocation; set => currentLocation = value; }

        public WeatherConditions()
        {
        }

        public WeatherConditions(string location, int threshold)
        {
            CurrentLocation = location;
            TempThreshold = threshold;
        }

        public WeatherConditions(SerializationInfo info, StreamingContext context)
        {
            CurrentLocation = info.GetString("CurrentLocation");
            TempThreshold = info.GetInt32("TempThreshold");
        }


        public override bool Check()
        {
            try
            {
                var weatherEngine = new WeatherEngine(CurrentLocation);
                var currentWeather = weatherEngine.GetCurrentWeather();
                var currentImgUrl = weatherEngine.ImgUrl;

                if (weatherEngine.Weather.Main.Temp < TempThreshold)
                {
                    CheckInformation = $"Warunek niespełniony! Temperatura poniżej podanego poziomu. Aktualna: {weatherEngine.Weather.Main.Temp}";
                    return false;
                }

                var info = new ImageInfo
                {
                    ImgUrl = currentImgUrl,
                    Description = currentWeather
                };

                result = info;

                return true;
            }
            catch (System.Net.WebException)
            {
                CheckInformation = $"Nie udało się znaleźć pogody dla miasta {CurrentLocation}";
                return false;
            }
            catch (Exception ex)
            {
                throw new JtttException("Nie udało się pobrać danych pogodowych!", "Exception : WeatherConditions::Check()", ex);
            }
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("WeatherConditions : SerializationInfo");
            info.AddValue("CurrentLocation", CurrentLocation, CurrentLocation.GetType());
            info.AddValue("TempThreshold", TempThreshold, TempThreshold.GetType());
        }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (!isEmptyElement)
            {
                CurrentLocation = reader.ReadElementString("CurrentLocation");
                TempThreshold = Int32.Parse(reader.ReadElementString("TempThreshold"));
                reader.ReadEndElement();
            }
        }

        public override string ToString()
        {
            return $"[Wheather in {CurrentLocation} is greater than {TempThreshold}]";
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("CurrentLocation", CurrentLocation);
            writer.WriteElementString("TempThreshold", TempThreshold.ToString());
        }
    }
}
