﻿using System;
using System.Linq;
using System.Text;
using System.Net;
using System.Runtime.Serialization;
using HtmlAgilityPack;
using System.Xml;

namespace dotnet_jttt_PM_HK.Conditions
{
    using Interfaces;
    using Exceptions;
    using Models;

    [Serializable]
    public class ImageFinder : ICondition
    {
        private string _url;
        private string _alt;

        public string Url { get => _url; set => _url = value; }
        public string Alt { get => _alt; set => _alt = value; }

        public ImageFinder()
        { }

        public ImageFinder(string url, string alt)
        {
            this._url = url;
            this._alt = alt;
        }

        public ImageFinder(SerializationInfo info, StreamingContext context)
        {
            this._url = info.GetString("url");
            this._alt = info.GetString("alt");
        }

        private string GetHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(_url));

                return html;
            }
        }

        private string FindFirstImageUrl()
        {
            var doc = new HtmlDocument();
            var pageHtml = GetHtml();
            doc.LoadHtml(pageHtml);
            var nodes = doc.DocumentNode.Descendants("img");
            var findNode = nodes.FirstOrDefault(
                node =>
                {
                    string alt = node.GetAttributeValue("alt", "");
                    string src = node.GetAttributeValue("src", "");
                    Console.WriteLine(node.GetAttributeValue("href",""));
                    return alt.Contains(_alt) && src.Contains("http");
                });
            if (findNode is null)
            {
                var nodesWithHref = doc.DocumentNode.Descendants("a");
                var newFindNode = nodesWithHref.FirstOrDefault(node =>
                   {
                       string href = node.GetAttributeValue("href", "");
                       href = href.Split('/').Last().Replace('-', ' ');
                       if (href.Contains(_alt) && node.HasChildNodes)
                       {
                            foreach (var iNode in node.ChildNodes)
                            {
                                var src = iNode.GetAttributeValue("src", "");
                                if (src != "" && src.Contains("http"))
                                    return true;
                            }
                        }
                       return false;
                   });
                if(newFindNode != null)
                {
                    foreach(var iNode in newFindNode.ChildNodes)
                    {
                        var src = iNode.GetAttributeValue("src", "");
                        if (src != "" && src.Contains("http"))
                            return src;
                    }
                }
                return "";
            }
            return findNode.GetAttributeValue("src", "");
        }

        public override bool Check()
        {
            try
            {
                result = FindFirstImageUrl();
                if ((string)result == "")
                {
                    CheckInformation = "Nie znaleziono obrazka o podanym tekście";
                    return false;
                }
                var info = new ImageInfo
                {
                    ImgUrl = (string)result,
                    Description = ""
                };
                result = info;
                return true;
            }
            catch (Exception ex)
            {
                throw new JtttException("Nie udało się połączyć ze stroną " + Url, "Exception : ImageFinder::Check()", ex);
            }
        }

        public override string ToString()
        {
            return $"[Find Image Url: {Url} Text: {Alt}]";
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("ImageFinder : SerializationInfo");
            info.AddValue("url", _url, _url.GetType());
            info.AddValue("alt", _alt, _alt.GetType());
        }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (!isEmptyElement)
            {
                _url = reader.ReadElementString("url");
                _alt = reader.ReadElementString("alt");
                reader.ReadEndElement();
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("url", _url);
            writer.WriteElementString("alt", _alt);
        }
    }
}
