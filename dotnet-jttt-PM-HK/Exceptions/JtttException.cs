﻿using System;
using System.Runtime.Serialization;

namespace dotnet_jttt_PM_HK.Exceptions
{
    [Serializable]
    public class JtttException : Exception
    {
        private string userInfo;
        private string logInfo;

        public string UserInfo
        {
            get => userInfo;
            set => userInfo = value;
        }

        public string LogInfo
        {
            get
            {
                var message = "";
                if (InnerException is null)
                {
                    message = $"|*| JtttException information: \n" +
                              $"|*| Message: {logInfo} \n" +
                              $"|*| Stack trace: \n {StackTrace} \n";
                }
                else
                {
                    message = $"|*| Inner Exception information \n" +
                              $"|*| Type: {InnerException?.GetType()} \n" +
                              $"|*| Message: {InnerException?.Message} \n" +
                              $"|*| Stack trace: \n {InnerException?.StackTrace} \n" +
                              $"|*| JtttException information: \n" +
                              $"|*| Message: {logInfo} \n" +
                              $"|*| Stack trace: \n {StackTrace} \n";
                }
                return message.Replace("\n", Environment.NewLine);

            }
            set => logInfo = value;
        }

        public JtttException(string userInfo, string logInfo)
        {
            this.userInfo = userInfo;
            this.logInfo = logInfo;
        }

        public JtttException(string userInfo, string logInfo, Exception inner)
            :base("", inner)
        {
            this.userInfo = userInfo;
            this.logInfo = logInfo;
        }

        protected JtttException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            userInfo = info.GetString("UserInfo");
            logInfo = info.GetString("LogInfo");
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");
            info.AddValue("UserInfo", UserInfo);
            info.AddValue("LogInfo", LogInfo);
            base.GetObjectData(info, context);
        }
    }
}
