﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace dotnet_jttt_PM_HK.Static
{
    static public class ConcreteSmtpClient
    {
        static private SmtpClient _client = new SmtpClient();
        static private string _from = "jtttdev@gmail.com";
        static private string _pass = "codehardjttt";

        static ConcreteSmtpClient()
        {
            _client.Port = 587;
            _client.Host = "smtp.gmail.com";
            _client.EnableSsl = true;
            _client.Timeout = 10000;
            _client.DeliveryMethod = SmtpDeliveryMethod.Network;
            _client.UseDefaultCredentials = false;
            _client.Credentials = new System.Net.NetworkCredential(_from, _pass);
        }

        static public void SendAttachment(string PathToAttachment, string to, string body = "")
        {
            MailMessage mail = new MailMessage
            {
                IsBodyHtml = false,
                Body = body,
                From = new MailAddress(_from, "To ja Mario!")
            };
            var attachment = new Attachment(PathToAttachment);
            mail.Attachments.Add(attachment);
            mail.To.Add(to);
            mail.Subject = "Wspaniała wiadomość!";
            _client.Send(mail);
            attachment.Dispose();
        }

        static public void SendHtml(string to, string body = "")
        {
            MailMessage mail = new MailMessage
            {
                IsBodyHtml = true,
                Body = body,
                From = new MailAddress(_from, "To ja Mario!")
            };
            mail.To.Add(to);
            mail.Subject = "Wspaniała wiadomość!";
            _client.Send(mail);
        }
    }
}
