﻿using dotnet_jttt_PM_HK.Windows;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Controls;

namespace dotnet_jttt_PM_HK.Static
{
    public static class Logger
    {
        private static readonly string _filePath = "log.dump";
        private static readonly string _filename = _filePath.Split('\\').Last();
        private static BindingList<string> windowLogs = new BindingList<string>();
        private static MainWindow window = null;
        

        public static string Filename => _filename;
        public static BindingList<string> WindowLogs { get => windowLogs; }

        static Logger()
        {
        }

        public static void Log(string message)
        {
            using (StreamWriter streamWriter = new StreamWriter(_filePath, append: true))
            {
                message = $"****************************************************** \n" +
                          $"{DateTime.Now.ToString("[dd-MM-yyyy] [HH:mm:ss] : ")} \n" +
                          $"{message} \n";
                streamWriter.WriteLine(message.Replace("\n", Environment.NewLine));
                streamWriter.Close();
            }
        }

        internal static void SetupWindowLogs(MainWindow window)
        {
            Logger.window = window;
            Logger.window.lbLogs.DataContext = windowLogs;
        }

        public static void WindowLog(string message)
        {
            window.Dispatcher.Invoke( 
                () =>
                {
                    message = $"[{windowLogs.Count}] {message}";
                    windowLogs.Add(message);
                    window.lbLogs.SelectedIndex = window.lbLogs.Items.Count - 1;
                    window.lbLogs.ScrollIntoView(window.lbLogs.SelectedItem);
                });
        }
    }
}
