﻿using System.Data.Entity;

namespace dotnet_jttt_PM_HK.Static
{
    using Models;
    using Interfaces;

    public class TaskDbContext: DbContext 
    {
       public TaskDbContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<TaskDbContext>());
        }

        public DbSet<Task> Tasks { get; set; }
        public DbSet<IAction> Actions { get; set; }
        public DbSet<ICondition> Conditions { get; set; }
    }
}
