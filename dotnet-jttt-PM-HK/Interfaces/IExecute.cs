﻿namespace dotnet_jttt_PM_HK.Interfaces
{
    public abstract class IExecute
    {
        private string executeInformation = "";
        public abstract bool Execute();
        public virtual string ExecuteInformation
        {
            get
            {
                if (executeInformation == "")
                    return null;
                var tmp = executeInformation;
                executeInformation = "";
                return tmp;
            }
            set { executeInformation = value; }
        }
    }
}
