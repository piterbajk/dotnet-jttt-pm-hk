﻿using dotnet_jttt_PM_HK.Models;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace dotnet_jttt_PM_HK.Interfaces
{
    [Serializable]
    public abstract class ICondition : ISerializable, IXmlSerializable
    {
        public int ID { get; set; }

        protected object result;
        public object Result { get => result; }
        public abstract bool Check();
        public string CheckInformation { get; set; }

        public new abstract string ToString();
        public abstract void GetObjectData(SerializationInfo info, StreamingContext context);
        public abstract void ReadXml(XmlReader reader);
        public abstract void WriteXml(XmlWriter writer);
        public XmlSchema GetSchema()
        {
            return null;
        }
    }
}
