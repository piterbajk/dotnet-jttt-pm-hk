﻿using System;

namespace dotnet_jttt_PM_HK.Interfaces
{
    public interface IUCCondition
    {
        ICondition GetCondition();
        bool FieldsAreValid();
        Type ConditionResultType();
    }
}
