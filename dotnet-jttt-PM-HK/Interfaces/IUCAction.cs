﻿using System;

namespace dotnet_jttt_PM_HK.Interfaces
{
    public interface IUCAction
    {
        IAction GetAction();
        bool FieldsAreValid();
        Type ActionArgsType();
    }
}
