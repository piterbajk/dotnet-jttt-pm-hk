﻿using System;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace dotnet_jttt_PM_HK.Interfaces
{
    [Serializable]
    public abstract class ITask : IExecute, ISerializable, IXmlSerializable
    {
        public int ID { get; set; }
        public string TaskName { get; set; }
        public string ViewInfo { get; set; }

        public override abstract bool Execute();

        public abstract void GetObjectData(SerializationInfo info, StreamingContext context);
        public abstract void ReadXml(XmlReader reader);
        public abstract void WriteXml(XmlWriter writer);
        public XmlSchema GetSchema()
        {
            return null;
        }
    }
}
