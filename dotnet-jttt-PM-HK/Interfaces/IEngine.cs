﻿using System;
using System.ComponentModel;

namespace dotnet_jttt_PM_HK.Interfaces
{
    public abstract class IEngine : IExecute 
    {
        private BindingList<ITask> taskList = new BindingList<ITask>();

        public virtual BindingList<ITask> TaskList { get => taskList; set => taskList = value; }

        public abstract bool AddToList(ITask task);
        public abstract override bool Execute();
        public abstract void Erase();
        public abstract bool Load();
        public abstract bool Save();
        internal abstract bool LoadFromDb();
        internal abstract bool EraseDb();
    }
}
