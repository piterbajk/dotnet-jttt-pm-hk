﻿using System;
using System.Linq;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Xml;

namespace dotnet_jttt_PM_HK.Actions
{
    using Interfaces;
    using Exceptions;
    using Static;
    using Models;

    [Serializable]
    public class AttachmentSender : IAction
    {
        private static readonly string _folder = CreatTempAttachmentFolder();
        private string _to;

        public string To { get => _to; set => _to = value; }

        public AttachmentSender()
        {
        }

        public AttachmentSender(string to)
        {
            _to = to;
        }

        public AttachmentSender(SerializationInfo info, StreamingContext context)
        {
            _to = info.GetString("to");
        }

        private static string CreatTempAttachmentFolder()
        {
            var path = Path.Combine(Directory.GetCurrentDirectory() ,"/AttachmentTemp/");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            return path;
        }

        private void DeleteTempAttachment(string fullpath)
        {
            if (Directory.Exists(Path.GetDirectoryName(fullpath)))
            {
                File.Delete(fullpath);
            }
        }

        private string CreatAndGetAttachment(string url)
        {
            var name = url.Split('/').Last();
            if (!(name.Contains(".jpg") || name.Contains(".png") || name.Contains(".gif")))
                name += ".jpg";
            var fullpath = Path.Combine(_folder,name);
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(new Uri(url), fullpath);
            }
            return fullpath;
        }

        public override void Perform(object args)
        {

            var imgInfo = args as ImageInfo;
            if (imgInfo is null)
                throw new JtttException($"Nie udało się wysłać wiadomości do {To}", "Exception : AttachmentSender::Perform()\n Arrgument imgUrl is null");
            try
            {
                var path = CreatAndGetAttachment(imgInfo.ImgUrl);
                ConcreteSmtpClient.SendAttachment(path, _to, imgInfo.Description);
                DeleteTempAttachment(path);
            }
            catch (Exception ex)
            {
                throw new JtttException($"Nie udało się wysłać wiadomości do {To}", "Exception : AttachmentSender::Perform()", ex);
            }
        }

        public override string ToString()
        {
            return $"[Send Image with attachment To: {To}]";
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("to", _to, _to.GetType());
        }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (!isEmptyElement)
            {
                _to = reader.ReadElementString("to");
                reader.ReadEndElement();
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("to", _to);
        }
    }
}
