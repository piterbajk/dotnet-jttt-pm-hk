﻿using System;
using System.Runtime.Serialization;
using System.Xml;

namespace dotnet_jttt_PM_HK.Actions
{
    using Interfaces;
    using Exceptions;
    using Static;
    using Models;

    [Serializable]
    public class ImageSender : IAction
    {
        private static readonly string templateBody =
           @"<html>
                <body>
                    <table width=""100%"">
                        <tr>
                            <td style=""font-style:arial; color:black; font-weight:bold"">
                                <p>#bodyID#</p>
                            </td>
                        </tr>
                        <tr>
                            <td style=""font-style:arial; color:black; font-weight:bold"">
                                <img src=""#myImageID#"">
                            </td>
                        </tr>
                    </table>
                </body>
            </html>";

        private string _to;


        public string To { get => _to; set => _to = value; }

        public ImageSender()
        {
        }

        public ImageSender(string to)
        {
            _to = to;
        }

        public ImageSender(SerializationInfo info, StreamingContext context)
        {
            _to = info.GetString("to");
        }

        public override void Perform(object args)
        {
            var imgInfo = args as ImageInfo;
            if (imgInfo is null)
                throw new JtttException($"Nie udało się wysłać wiadomości do {To}", "Exception : ImageSender::Perform()\n Arrgument imgInfo is null");
            try
            {
                ConcreteSmtpClient.SendHtml(_to, templateBody.Replace("#myImageID#", imgInfo.ImgUrl).Replace("#bodyID#", imgInfo.Description));
            }
            catch (Exception ex)
            {
                throw new JtttException($"Nie udało się wysłać wiadomości do {To}", "Exception : ImageSender::Perform()", ex);
            }
        }

        public override string ToString()
        {
            return $"[Send Image To: {To}]";
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("ImageSender : SerializationInfo");
            info.AddValue("to", _to, _to.GetType());
        }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (!isEmptyElement)
            {
                _to = reader.ReadElementString("to");
                reader.ReadEndElement();
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("to", _to);
        }
    }
}
