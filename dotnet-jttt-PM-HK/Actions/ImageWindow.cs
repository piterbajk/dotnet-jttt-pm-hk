﻿using System;
using System.Runtime.Serialization;
using System.Xml;
using System.ComponentModel.DataAnnotations.Schema;

namespace dotnet_jttt_PM_HK.Actions
{
    using Interfaces;
    using Exceptions;
    using Models;
    using dotnet_jttt_PM_HK.Windows;

    [Serializable]
    public class ImageWindow : IAction
    {
        [NonSerialized]
        private MainWindow window;

        [NotMapped]
        public MainWindow Window { get => window; set => window = value; }

        public override void Perform(object args)
        {
            var imgInfo = args as ImageInfo;
            if (imgInfo is null)
                throw new JtttException($"Nie udało się wyświetlić zdjęcia", "Exception : ImageWindow::Perform()\n Arrgument imgInfo is null");
            try
            {
                if (Window == null)
                    new Windows.ImageWindow(imgInfo).Show();
                else
                    Window.Dispatcher.InvokeAsync(() => { new Windows.ImageWindow(imgInfo).Show(); });
            }
            catch (Exception ex)
            {
                throw new JtttException($"Nie udało się wyświetlić zdjęcia", "Exception : ImageSender::Perform()", ex);
            }
        }

        public override string ToString()
        {
            return $"[Display image]";
        }

        public ImageWindow()
        {
        }

        public ImageWindow(SerializationInfo info, StreamingContext context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
        }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (!isEmptyElement)
            {
                reader.ReadEndElement();
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
        }
    }
}
