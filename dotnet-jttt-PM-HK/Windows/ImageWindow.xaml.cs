﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace dotnet_jttt_PM_HK.Windows
{
    using Models;

    /// <summary>
    /// Interaction logic for ImageWindow.xaml
    /// </summary>
    public partial class ImageWindow : Window
    {
        public ImageWindow(ImageInfo info)
        {
            InitializeComponent();

            BeginInit();
            if(info.Description is null || info.Description == "")
            {
                tbDescribtion.Visibility = Visibility.Hidden;
                tbDescribtion.Height = 0;
                tbConstDescribtion.Visibility = Visibility.Hidden;
                tbConstDescribtion.Height = 0;

            }
            else
            {
                tbDescribtion.Text = info.Description;
            }
            var bitmap = new BitmapImage(new Uri(info.ImgUrl));
            imgField.Source = bitmap;
            EndInit();
        }
    }
}
