﻿using System.Windows;
using System;
using System.Linq;
using System.Windows.Controls;
using System.Collections.Generic;

namespace dotnet_jttt_PM_HK.Windows
{
    using Interfaces;
    using Models;
    using Static;
    using Exceptions;
    using dotnet_jttt_PM_HK.Controls;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Dictionary<string, IUCCondition> _conditionMap;
        private readonly Dictionary<string, IUCAction> _actionMap;
        private readonly IEngine _engine;

        public MainWindow(Dictionary<string, IUCCondition> conditionMap, Dictionary<string, IUCAction> actionMap,IEngine engine)
        {
            _conditionMap = conditionMap;
            _actionMap = actionMap;
            _engine = engine;

            InitializeComponent();
            _engine.LoadFromDb();

            ucCondition.Content = _conditionMap.First().Value;
            ucAction.Content = _actionMap.First().Value;
            lbTasks.DataContext = _engine.TaskList;
            ucWeatherInfo.Content = new ucWeatherInfo();
            Logger.SetupWindowLogs(this);
            Logger.WindowLog("Start aplikacji");
        }

        private void CmbCondition_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            combo.ItemsSource = new List<string>(_conditionMap.Keys);
            combo.SelectedIndex = 0;
        }

        private void CmbCondition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var combo = sender as ComboBox;
            string name = combo.SelectedItem as string;
            if(_conditionMap[name].ConditionResultType() == ((IUCAction)ucAction.Content).ActionArgsType())
            {
                ucCondition.Content = _conditionMap[name];
            }
            else
            {
                combo.SelectedItem = _conditionMap.First(x => x.Value.Equals(ucCondition.Content)).Key;
                MessageBox.Show("Nie można połączyć tego warunku i akcji!", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void CmbAction_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            combo.ItemsSource = new List<string>(_actionMap.Keys);
            combo.SelectedIndex = 0;
        }

        private void CmbAction_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var combo = sender as ComboBox;
            string name = combo.SelectedItem as string;
            if (_actionMap[name].ActionArgsType() == ((IUCCondition)ucCondition.Content).ConditionResultType())
            {
                ucAction.Content = _actionMap[name];
            }
            else
            {
                combo.SelectedItem = _actionMap.First(x => x.Value.Equals(ucAction.Content)).Key;
                MessageBox.Show("Nie można połączyć tego warunku i akcji!", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void SecureCall(Action action)
        {
            try
            {
                action();
            }
            catch(JtttException ex)
            {
                var info = _engine.ExecuteInformation;
                if(info is null)
                    MessageBox.Show($"{ex.UserInfo}", "Info", MessageBoxButton.OK, MessageBoxImage.Warning);
                else
                    MessageBox.Show($"{info}\n{ex.UserInfo}", "Info", MessageBoxButton.OK, MessageBoxImage.Warning);
                Logger.Log(ex.LogInfo);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Polecenie nie może zostać wykonane\nWystąpił wewnętrzny błąd\n Przepraszamy!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                Logger.Log($@"Exeption raise : [{ex.Message}]");
            }
        }

        private void BtnAddToListOnClick(object sender, RoutedEventArgs e)
        {
            SecureCall( () =>
            {
                var taskName = tbTaskName.Text;
                var condition = ((IUCCondition)ucCondition.Content).GetCondition();
                var action = ((IUCAction)ucAction.Content).GetAction();
                if (condition is null || action is null || taskName == "")
                {
                    MessageBox.Show("Pola nie mogą być puste!", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                if (!_engine.AddToList(new Task(taskName, condition, action)))
                {
                    MessageBox.Show("Zadanie nie zostało dodane do listy\nponieważ zadanie o tej nazwie juz istnieje", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    Logger.WindowLog($"Dodano nowe zadanie {taskName}");
                }
            });

        }

        private void BtnExecuteOnClick(object sender, RoutedEventArgs e)
        {
            SecureCall(() =>
            {
                if (_engine.TaskList.Count == 0)
                {
                    MessageBox.Show("Brak akcji do wykonania!", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                Logger.WindowLog($"Rozpoczęto wykonywać listę zadań");
                ((Engine)_engine).ExecuteAsync(this);
            });
        }

        private void BtnEraseOnClick(object sender, RoutedEventArgs e)
        {
            SecureCall(() =>
            {
                if (_engine.TaskList.Count != 0)
                {
                    _engine.Erase();
                    Logger.WindowLog($"Wyczyszczono listę zadań");
                }
            });
        }

        private void BtnLoadOnClick(object sender, RoutedEventArgs e)
        {
            SecureCall(() =>
            {
                if(_engine.Load())
                    MessageBox.Show("Lista została wczytana!", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            });
        }

        private void BtnSaveOnClick(object sender, RoutedEventArgs e)
        {
            SecureCall(() =>
            {
                if (_engine.TaskList.Count == 0)
                {
                    MessageBox.Show("Nie ma nic do zapisu\nLista jest pusta", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                if (_engine.Save())
                    MessageBox.Show("Lista została zapisana!", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            });
        }
    }
}