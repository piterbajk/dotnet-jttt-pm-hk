﻿using System;
using System.Windows.Controls;

namespace dotnet_jttt_PM_HK.Controls
{
    using Interfaces;
    using Actions;
    using Models;
    /// <summary>
    /// Interaction logic for ucImageWindow.xaml
    /// </summary>
    public partial class ucImageWindow : UserControl, IUCAction
    {
        public ucImageWindow()
        {
            InitializeComponent();
        }

        public Type ActionArgsType()
        {
            return typeof(ImageInfo);
        }

        public bool FieldsAreValid()
        {
            return true;
        }

        public IAction GetAction()
        {
            if (FieldsAreValid())
                return new ImageWindow();
            else
                return null;
        }
    }
}
