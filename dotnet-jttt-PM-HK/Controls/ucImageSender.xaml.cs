﻿using System;
using System.Windows.Controls;

namespace dotnet_jttt_PM_HK.Controls
{
    using Interfaces;
    using Actions;
    using Models;
    /// <summary>
    /// Interaction logic for ucImageSender.xaml
    /// </summary>
    public partial class ucImageSender : UserControl, IUCAction
    {
        public ucImageSender()
        {
            InitializeComponent();
        }

        public Type ActionArgsType()
        {
            return typeof(ImageInfo);
        }

        public bool FieldsAreValid()
        {
            if (tbAdres.Text == "")
                return false;
            return true;
        }

        public IAction GetAction()
        {
            if (FieldsAreValid())
                return new ImageSender(tbAdres.Text);
            else
                return null;
        }
    }
}
