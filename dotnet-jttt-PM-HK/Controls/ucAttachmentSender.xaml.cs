﻿using System;
using System.Windows.Controls;

namespace dotnet_jttt_PM_HK.Controls
{
    using Interfaces;
    using Actions;
    using Models;
    /// <summary>
    /// Interaction logic for ucAttachmentSender.xaml
    /// </summary>
    public partial class ucAttachmentSender : UserControl, IUCAction
    {
        public ucAttachmentSender()
        {
            InitializeComponent();
        }

        public Type ActionArgsType()
        {
            return typeof(ImageInfo);
        }

        public bool FieldsAreValid()
        {
            if (tbAdres.Text == "")
                return false;
            return true;
        }

        public IAction GetAction()
        {
            if (FieldsAreValid())
                return new AttachmentSender(tbAdres.Text);
            else
                return null;
        }
    }
}
