﻿using System;
using System.Windows.Controls;

namespace dotnet_jttt_PM_HK.Controls
{
    using Interfaces;
    using Conditions;
    using Models;
    using System.Windows;

    /// <summary>
    /// Interaction logic for ucWeatherConditions.xaml
    /// </summary>
    public partial class ucWeatherConditions : UserControl, IUCCondition
    {
        private int CurrentThreshold = 15;

        public ucWeatherConditions()
        {
            InitializeComponent();
            tbThreshold.Text = CurrentThreshold.ToString();
        }

        public Type ConditionResultType()
        {
            return typeof(ImageInfo);
        }

        public bool FieldsAreValid()
        {
            if (tbLocation.Text == "" || tbThreshold.Text == "")
                return false;
            return true;
        }

        public ICondition GetCondition()
        {
            if (FieldsAreValid())
                return new WeatherConditions(tbLocation.Text, Int32.Parse(tbThreshold.Text));
            else
                return null;
        }
        private void BtnIncOnClick(object sender, RoutedEventArgs e)
        {
            tbThreshold.Text = (++CurrentThreshold).ToString();
        }
        private void BtnDecOnClick(object sender, RoutedEventArgs e)
        {
            tbThreshold.Text = (--CurrentThreshold).ToString();
        }
    }
}
