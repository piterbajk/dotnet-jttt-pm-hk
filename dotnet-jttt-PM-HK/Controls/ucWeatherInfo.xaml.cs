﻿using dotnet_jttt_PM_HK.Exceptions;
using dotnet_jttt_PM_HK.Models;
using dotnet_jttt_PM_HK.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace dotnet_jttt_PM_HK.Controls
{
    /// <summary>
    /// Interaction logic for ucWeatherInfo.xaml
    /// </summary>
    public partial class ucWeatherInfo : UserControl
    {
        public ucWeatherInfo()
        {
            InitializeComponent();
        }

        public bool FieldsAreValid()
        {
            if (tbLocation.Text == "")
                return false;
            return true;
        }

        private void BtnCheckWeatherOnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var weatherEngine = new WeatherEngine(tbLocation.Text);
                tbWeatherDescription.Text = weatherEngine.GetCurrentWeather();

                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(weatherEngine.ImgUrl, UriKind.Absolute);
                bitmap.EndInit();

                imgWeatherImage.Source = bitmap;
            }
            catch (JtttException ex)
            {
                MessageBox.Show($"{ex.UserInfo}", "Info", MessageBoxButton.OK, MessageBoxImage.Warning);
                Logger.Log(ex.LogInfo);
            }
            catch (System.Net.WebException ex)
            {
                MessageBox.Show("Nie znaleziono podanego miasta!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                Logger.Log($@"Exeption raise : [{ex.Message}]");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Polecenie nie może zostać wykonane\nWystąpił wewnętrzny błąd\n Przepraszamy!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                Logger.Log($@"Exeption raise : [{ex.Message}]");
            }
        }
    }
}
