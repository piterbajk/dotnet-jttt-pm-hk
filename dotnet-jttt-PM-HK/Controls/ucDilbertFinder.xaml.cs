﻿using System;
using System.Windows.Controls;

namespace dotnet_jttt_PM_HK.Controls
{
    using Interfaces;
    using Conditions;
    using Models;
    /// <summary>
    /// Interaction logic for ucDilbertFinder.xaml
    /// </summary>
    public partial class ucDilbertFinder : UserControl, IUCCondition
    {
        public ucDilbertFinder()
        {
            InitializeComponent();
        }

        public Type ConditionResultType()
        {
            return typeof(ImageInfo);
        }

        public bool FieldsAreValid()
        {
            return true;
        }

        public ICondition GetCondition()
        {
            return new DilbertFinder();
        }
    }
}
