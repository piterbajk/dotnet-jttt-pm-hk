﻿using System;
using System.Windows.Controls;

namespace dotnet_jttt_PM_HK.Controls
{
    using Interfaces;
    using Conditions;
    using Models;
    /// <summary>
    /// Interaction logic for ucImageFinder.xaml
    /// </summary>
    public partial class ucImageFinder : UserControl, IUCCondition
    {
        public ucImageFinder()
        {
            InitializeComponent();
        }

        public Type ConditionResultType()
        {
            return typeof(ImageInfo);
        }

        public bool FieldsAreValid()
        {
            if (tbTekst.Text == "" || tbUrl.Text == "")
                return false;
            return true;
        }

        public ICondition GetCondition()
        {
            if(FieldsAreValid())
                return new ImageFinder(tbUrl.Text, tbTekst.Text);
            else
                return null;
        }
    }
}
