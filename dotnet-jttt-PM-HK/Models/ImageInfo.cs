﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnet_jttt_PM_HK.Models
{
    public class ImageInfo
    {
        public string Description { get; set; }
        public string ImgUrl { get; set; }
    }
}
