﻿using System;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace dotnet_jttt_PM_HK.Models
{

    using Interfaces;

    [Serializable]
    public class Task : ITask
    {
        private ICondition _condition;
        private IAction _action;

        public virtual ICondition Condition { get => _condition; set => _condition = value;  }
        public virtual IAction Action { get => _action; set => _action = value; }

        public Task() { }

        public Task(string taskName, ICondition condition, IAction action)
        {
            TaskName = taskName;
            _condition = condition;
            _action = action;
        }

        public Task(SerializationInfo info, StreamingContext context)
        {
            TaskName = info.GetString("TaskName");
            _condition = (ICondition)info.GetValue("condition", typeof(ICondition));
            _action = (IAction)info.GetValue("action", typeof(IAction));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("Task : SerializationInfo");
            info.AddValue("TaskName", TaskName, TaskName.GetType());
            info.AddValue("condition", _condition, _condition.GetType());
            info.AddValue("action", _action, _action.GetType());
        }

        public override bool Execute()
        {
            if (_condition.Check())
            {
                _action.Perform(_condition.Result);
                return true;
            }
            else
            {
                ExecuteInformation = _condition.CheckInformation;
                return false;
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            if (!reader.HasAttributes)
                throw new FormatException("Expected a type attribute!");

            var conditionName = reader.GetAttribute("Condition");
            if (conditionName == "dotnet_jttt_PM_HK.Conditions.ImageFinder")
                _condition = new Conditions.ImageFinder();
            else if (conditionName == "dotnet_jttt_PM_HK.Conditions.DilbertFinder")
                _condition = new Conditions.DilbertFinder();
            else if (conditionName == "dotnet_jttt_PM_HK.Conditions.WeatherConditions")
                _condition = new Conditions.WeatherConditions();
            else
                throw new ArgumentException("Unrecognized Condition Name");

            var actionName = reader.GetAttribute("Action");
            if (actionName == "dotnet_jttt_PM_HK.Actions.ImageSender")
                _action = new Actions.ImageSender();
            else if (actionName == "dotnet_jttt_PM_HK.Actions.AttachmentSender")
                _action = new Actions.AttachmentSender();
            else if (actionName == "dotnet_jttt_PM_HK.Actions.ImageWindow")
                _action = new Actions.ImageWindow();
            else
                throw new ArgumentException("Unrecognized Action Name");

            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (!isEmptyElement)
            {
                TaskName = reader.ReadElementString("TaskName");
                _condition.ReadXml(reader);
                _action.ReadXml(reader);
                reader.ReadEndElement();
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("Condition", _condition.GetType().ToString());
            writer.WriteAttributeString("Action", _action.GetType().ToString());
            writer.WriteElementString("TaskName", TaskName);
            XmlSerializer conditionFormatter = new XmlSerializer(_condition.GetType());
            conditionFormatter.Serialize(writer, _condition);
            XmlSerializer actionFormatter = new XmlSerializer(_action.GetType());
            actionFormatter.Serialize(writer, _action);
        }

        public new string ViewInfo => $@"{TaskName} {Condition.ToString()} {Action.ToString()}";
    }
}
