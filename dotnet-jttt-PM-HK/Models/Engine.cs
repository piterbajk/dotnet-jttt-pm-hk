﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Win32;
using System;
using System.Xml.Serialization;

namespace dotnet_jttt_PM_HK.Models
{
    using Interfaces;
    using Static;
    using Exceptions;
    using dotnet_jttt_PM_HK.Utils;
    using System.Windows;
    using dotnet_jttt_PM_HK.Windows;

    public class Engine : IEngine
    {

        public override bool AddToList(ITask task)
        {
            if (TaskList.FirstOrDefault(x => x.TaskName == task.TaskName) is null)
            {
                TaskList.Add(task);
                using (var db = new TaskDbContext())
                { 
                    db.Tasks.Add((Task)task);
                    db.SaveChanges();
                }
            }
            else
                return false;
            return true;
        }

        internal override bool LoadFromDb()
        {
            try
            {
                using (var db = new TaskDbContext())
                {
                    var tasks = db.Tasks
                        .Include("Condition")
                        .Include("Action")
                        .ToList();
                    foreach (var task in tasks)
                    {
                        TaskList.Add(task);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new JtttException("Nie udało się pobrać danych z bazy.", "Exception : DBLoadError", ex);
            }

            return true;
        }

        internal override bool EraseDb()
        {
            try
            {
                using (var db = new TaskDbContext())
                {
                    var queryTasks = from t in db.Tasks select t;
                    foreach (var task in queryTasks)
                        db.Tasks.Remove(task);

                    var queryActions = from t in db.Actions select t;
                    foreach (var action in queryActions)
                        db.Actions.Remove(action);

                    var queryConditions = from t in db.Conditions select t;
                    foreach (var condition in queryConditions)
                        db.Conditions.Remove(condition);

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new JtttException("Nie udało się wyczyścić bazy.", "Exception : DBLoadError", ex);
            }

            return true;
        }

        private static bool ExecuteList(List<ITask> taskList, ref string executeInformation)
        {
            bool status = true;
            foreach (var task in taskList)
            {
                try
                {
                    if (!task.Execute())
                    {
                        var info = task.ExecuteInformation;
                        if (info is null)
                            executeInformation += $"Nie udało się wykonać {task.TaskName}\n";
                        else
                            executeInformation += $"Nie udało się wykonać {task.TaskName}, ponieważ {info}\n";
                        status = false;
                    }
                }
                catch (JtttException ex)
                {
                    var message = $"Wystąpił błąd przy wykonywaniu listy zadań\n" +
                                  $"Przerwano na zadniu: {task.TaskName}\n" +
                                  $"Powód: {ex.UserInfo}";
                    ex.UserInfo = message.Replace("\n", Environment.NewLine);
                    Logger.WindowLog($"Nie udało wykonywać wszystkich zadań");
                    throw;
                }
            }
            return status;
        }

        private static void ExecuteAsyncWrapper(object threadContext)
        {
            System.Threading.Thread.Sleep(5000);
            var executeObject = threadContext as Tuple<List<ITask>, MainWindow>;
            var taskList = executeObject.Item1;
            var mainWindow = executeObject.Item2;

            foreach(var task in taskList)
            {
                var tmpImageWindowAction = ((Task)task)?.Action as Actions.ImageWindow;
                if (tmpImageWindowAction != null)
                    tmpImageWindowAction.Window = mainWindow;
            }

            string executeInformation = "";
            try
            {
                if (ExecuteList(taskList, ref executeInformation))
                {
                    MessageBox.Show("Akcja wykonana pomyślnie!", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                    Logger.WindowLog($"Wszystkie zadania wykonane pomyślnie");
                }
                else
                {
                    Logger.WindowLog($"Nie udało wykonywać wszystkich zadań");
                    var info = executeInformation;
                    if (info != null)
                        MessageBox.Show(info, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                    else
                        throw new JtttException("Nie udało się wykonać wszystkich akcji", "ExecuteInformation don't set!");
                }
            }
            catch (JtttException ex)
            {
                var info = executeInformation;
                if (info is null)
                    MessageBox.Show($"{ex.UserInfo}", "Info", MessageBoxButton.OK, MessageBoxImage.Warning);
                else
                    MessageBox.Show($"{info}\n{ex.UserInfo}", "Info", MessageBoxButton.OK, MessageBoxImage.Warning);
                Logger.Log(ex.LogInfo);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Polecenie nie może zostać wykonane\nWystąpił wewnętrzny błąd\n Przepraszamy!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                Logger.Log($@"Exeption raise : [{ex.Message}]");
            }
        }

        public void ExecuteAsync(MainWindow mainWindow)
        {
            var copyTaskList = new List<ITask>();
            foreach (var task in TaskList)
            {
                copyTaskList.Add(task);
            };
            copyTaskList = ListUtils.CloneList(copyTaskList);
            var executeObject = new Tuple<List<ITask>, MainWindow>(copyTaskList,mainWindow);
            System.Threading.ThreadPool.QueueUserWorkItem(ExecuteAsyncWrapper, executeObject);
        }

        public override bool Execute()
        {
            bool status = true;
            foreach (var task in TaskList)
            {
                try
                {
                    if (!task.Execute())
                    {
                        var info = task.ExecuteInformation;
                        if (info is null)
                            ExecuteInformation += $"Nie udało się wykonać {task.TaskName}\n";
                        else
                            ExecuteInformation += $"Nie udało się wykonać {task.TaskName}, ponieważ {info}\n";
                        status = false;
                    }
                }
                catch(JtttException ex)
                {
                    var message = $"Wystąpił błąd przy wykonywaniu listy zadań\n" +
                                  $"Przerwano na zadniu: {task.TaskName}\n" +
                                  $"Powód: {ex.UserInfo}";
                    ex.UserInfo = message.Replace("\n", Environment.NewLine);
                    Logger.WindowLog($"Nie udało wykonywać wszystkich zadań");
                    throw;
                }
            }
            return status;
        }

        public override void Erase()
        {
            EraseDb();
            TaskList.Clear();
        }

        public override bool Load()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Title = "Wczytaj listę zadań",
                Filter = "Binary (*.bin)|*.bin|XML (*.xml)|*.xml"
            };
            openFileDialog.ShowDialog();
            var filename = openFileDialog.FileName;
            if (filename == null || filename == "")
                return false;
            try
            {
                var extension = filename.Split('.').Last();
                using (var db = new TaskDbContext())
                {
                    if (extension == "bin")
                    {
                        IFormatter formatter = new BinaryFormatter();
                        using (Stream inStream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            List<ITask> loadedTasks = (List<ITask>)formatter.Deserialize(inStream);
                            Erase();
                            foreach (ITask task in loadedTasks)
                            {
                                db.Tasks.Add((Task)task);
                                TaskList.Add(task);
                            }
                        }
                    }
                    else if (extension == "xml")
                    {
                        XmlSerializer formatter = new XmlSerializer(typeof(List<Task>));
                        using (Stream inStream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            List<Task> loadedTasks = (List<Task>)formatter.Deserialize(inStream);
                            Erase();
                            foreach (Task task in loadedTasks)
                            {
                                db.Tasks.Add((Task)task);
                                TaskList.Add(task);
                            }
                        }
                    }
                    db.SaveChanges();
                }
                Logger.WindowLog($"Wczytano listę zadań z pliku {filename}");
                return true;
            }
            catch(Exception ex)
            {
                throw new JtttException("Nie udało się wczytać danych\nPlik jest uszkodzony!", "Exception : Engine::Load()", ex);
            }
        }

        public override bool Save()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Title = "Zapisz listę zadań",
                Filter = "Binary (*.bin)|*.bin|XML (*.xml)|*.xml"
            };
            saveFileDialog.ShowDialog();
            var filename = saveFileDialog.FileName;
            if (filename == null || filename == "")
                return false;
            try
            {
                var extension = filename.Split('.').Last();
                if (extension == "bin")
                {
                    List<ITask> listToSerialize = new List<ITask>();
                    foreach (var task in TaskList)
                    {
                        listToSerialize.Add(task);
                    }
                    IFormatter formatter = new BinaryFormatter();
                    using (Stream outStream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        formatter.Serialize(outStream, listToSerialize);
                    }
                }
                else if (extension == "xml")
                {
                    List<Task> listToSerialize = new List<Task>();
                    foreach (var task in TaskList)
                    {
                        listToSerialize.Add((Task)task);
                    }
                    XmlSerializer formatter = new XmlSerializer(listToSerialize.GetType());
                    using (Stream outStream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        formatter.Serialize(outStream, listToSerialize);
                    }
                }
                Logger.WindowLog($"Zapisano listę zadań do pliku {filename}");
                return true;
            }
            catch (Exception ex)
            {
                throw new JtttException("Wystąpił problem z serializacją", "Exception : Engine::Save()", ex);
            }
        }
    }
}