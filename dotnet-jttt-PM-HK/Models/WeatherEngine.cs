﻿using System;
using System.Linq;
using System.Net;
using dotnet_jttt_PM_HK.Models.WeatherObject;
using Newtonsoft.Json;

namespace dotnet_jttt_PM_HK.Models
{
    class WeatherEngine
    {
        private string Url;
        private string APIKEY = "fce56c15cc912c50078f447adde90b8b";
        public string weatherConditions;
        public string ImgUrl;
        public Welcome Weather;

        public WeatherEngine(string location)
        {
            Url = $"http://api.openweathermap.org/data/2.5/weather?q={location}" +
                  $"&units=metric&APPID={APIKEY}";
        }

        public string GetCurrentWeather()
        {
                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString(Url);
                    Console.WriteLine(json);
                    Weather = JsonConvert.DeserializeObject<Welcome>(json);
                    weatherConditions = $"Dzisiaj jest {Weather.Main.Temp} stopni Celsjusza. Ciśnienie {Weather.Main.Pressure}. A co na niebie? {Weather.Weather.FirstOrDefault().Description}";
                    ImgUrl = "http://openweathermap.org/img/w/" + $"{Weather.Weather.FirstOrDefault().Icon}.png";
                    return weatherConditions;
                }
        }
    }
}
