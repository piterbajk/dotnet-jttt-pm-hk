﻿using System.Collections.Generic;
using System.Windows;

namespace dotnet_jttt_PM_HK
{
    using Interfaces;
    using Controls;
    using Models;
    using Windows;
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        void AppStartup(object sender, StartupEventArgs e)
        {
            var conditionMap = new Dictionary<string, IUCCondition>
            {
                {"Znajdź obrazek na stronie:", new ucImageFinder()},
                {"Znajdź dzisiejszy obrazek Dilberta", new ucDilbertFinder()},
                {"Znajdź prognozę pogody", new ucWeatherConditions()}
            };

            var actionMap = new Dictionary<string, IUCAction>
            {
                {"Wyślij zdjęcie e-mail:", new ucImageSender()},
                {"Wyślij e-mail z załącznikiem:", new ucAttachmentSender() },
                {"Wyświetl na ekranie", new ucImageWindow() }
            };

            var engine = new Engine();

            MainWindow window = new MainWindow(conditionMap, actionMap, engine);
            window.Show();
        }
    }
}
